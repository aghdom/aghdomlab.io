---
title: "Home"
date: 2019-02-25T17:37:37+01:00
author: Dominik Ágh
---

<!--DOCTOC SKIP-->

# Hello, friend

**Welcome to my blog!**

I honestly have no idea how you found this page, but since you are here, feel free to look around.
Most of the articles on this site will be tech oriented, but there might occasionally be a personal post, or something
regarding my hobbies (games, D&D, writing, etc.) Anyway, I hope you enjoy your stay!