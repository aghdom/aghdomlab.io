---
title: "Web Publishing 2nd Assignment"
date: 2019-03-25T18:49:51+01:00
technologies: []
tags: ["wpub","docbook","xml"]
---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [How I did it](#how-i-did-it)
    - [Chapters and Sections](#chapters-and-sections)
    - [Inserting Pictures or Figures](#inserting-pictures-or-figures)
    - [Cross-referencing](#cross-referencing)
    - [Bibliography and citations](#bibliography-and-citations)
    - [Auto-generated content](#auto-generated-content)
        - [Index](#index)
        - [Table of Content/Figures](#table-of-contentfigures)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

For my second assignment I had to convert my Bachelor's Project Report from LaTeX to DocBook.

# How I did it

Here are a few examples of how certain formatting and referencing is done in DocBook (version 4.3).

## Chapters and Sections

To separate different chapter and sections in DocBook you use indentically named tags `<chapter>` and `<section>`.
Each of those sections can also have a title, marked by a similarly named `<title>` tag. Each paragraphs of a section
should be marked with `<para>` tag. An example of a structured file could look like this:

```xml
  <chapter id="analyza">
  <title>Analýza problému</title>
    <para>
    ...
    </para>
    <section id="gene_algo">
        <title>Genetické algoritmy</title>
        <para>
        ...
        </para>
    </section>
  </chapter>
```

## Inserting Pictures or Figures

Here's an example of an inserted picture:

```xml
 <figure id="fig_gen_algo_steps">
  <title>Priebeh genetického algoritmu</title>
      <mediaobject>
        <imageobject>
          <imagedata fileref="BP1/fig/genetic_algo_steps.pdf" />
        </imageobject>
        <textobject>
          <phrase>Priebeh genetického algoritmu</phrase>
        </textobject>
      </mediaobject>
 </figure>
```

Here the most important parts are the `fileref` arg, which contains the address of the picture, and the `<textobject>` tag
allowing us to caption the image.

## Cross-referencing

To reference something in DocBook, you need to mark it using an `id` args. The actual referencing is then done using either
`<link>` or `<xref/>` tags, which contain `linkend` args pointing to the `id` of the referenced item. The difference is that
`<xref/>` automatically generates the reference text and `<link>` just makes its content point to the target. Here's an example:

```xml
  <!--Object to be referenced-->
  <figure id="my_fig">
    ...
  </figure>
  ...
  <!--Aut-generated reference to the object-->
  <xref linkend="my_fig" />

  <!--Custom reference link-->
  As shown in <link linkend="my_fig">this figure</link>...
```

## Bibliography and citations

Bibliography is a list of multiple `<biblioentry>` or `<bibliomixed>` objects placed in one `< bibliography>` object, which can have a `<title>`.
Citations are done the same way as cross-references, using a `<xref/>`, which points to an id in the entry.

## Auto-generated content

### Index

To index a term, add an `<indexterm>` on its page. To generate the index, place an `<index/>` tag in the desired location.
Here's an example:

```xml
  <!--single level index term-->
  <indexterm><primary>Loops</primary></indexterm>

  <!--multi level index term-->
  <indexterm><primary>Loops</primary><secondary>for<secondary/></indexterm>

  <!--Generate index-->
  <index/>
```

### Table of Content/Figures

`.xsl` to generate ToC/ToF:

```xml
  <xsl:param name="generate.toc">
  book      title,toc,figure
  </xsl:param>
```
