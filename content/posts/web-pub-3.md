---
title: "Web Publishing 3rd Assignment"
date: 2019-04-28T08:59:09+02:00
technologies: []
tags: ['wpub','xml','xlst','relaxng']
---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [How did I do it](#how-did-i-do-it)
    - [Technologies](#technologies)
        - [Schema](#schema)
        - [Transformation](#transformation)
        - [Styling](#styling)
    - [Document Structure](#document-structure)
        - [Basic structure](#basic-structure)
        - [Style elements](#style-elements)
        - [Parameters](#parameters)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

The task in my third school assignment was to design a schema for an XML document, and create an XSLT transformation which would convert the document
into a presentation.

# How did I do it

Here are some key features and technologies I used to create the final schema and transformation.

## Technologies

### Schema

To create the schema for the XML document I used **Relax NG Compact**, which is stored in and `schema.rnc` file. I linked it to the final document using
this line of code:

```xml
<?xml-model href="schema.rnc" type="application/relax-ng-compact-syntax"?>
```

Here is an example of how might the schema look like:

```rnc
start =
    element presentation{
        element slide {

            element header {
                element title {
                    styled_text
                }?,
                element subtitle {
                    styled_text
                }?
            },

            element body {
                element content {
                    attribute type {"text" | "image"},
                    styled_text
                }+
            }
        }+
    }
```

I preffered the Compact syntax over traditional Relax NG for the easy and speed of writing using it. To validate my documents I used `pyjing`, which is a
Python wrapper around the **Jing** Java Relax NG validator.

### Transformation

For the transformation of the XML into the final format, I used XSLT 2.0. The second version allows to create multiple final documents from a single XML file, which was very welcomed when creating this presentation. That however meant that I wasn't able to use `xsltproc` to transform the XML into the desired output, since `xsltproc` only supports XML 1.0. Because of that I had to use `saxon` for my transformation.

### Styling

For the visual side of the presentation **Bootstrap** was used.

## Document Structure

### Basic structure

The root element of the document is `<presentation/>`. It then contains individual `<slide/>` components, which are split into two parts `<header/>` and `<body/>`. The header can contain a `<title/>` and `<subtitle/>` elements, which can be styled using inline elements, which will be mentioned later.
The body can contain multiple `<content/>` elements, which can be of 3 different types:

- `text` - this is your basic text, possibly styled using different other inline elements
- `image` - this should be an URL or address of an image, which you wish to display.
- `video` - a URL of an embedded video.

If there is only one `<content/>` element, it will be stretched across the whole slide. Otherwise, the elements will be organised into 2 columns.

### Style elements

These are all the elements, which can be used to style text on the slides:

- `<bold/>` - Creates **bold** text
- `<italic/>` - Creates *italic* text
- `<underscore/>` - Creates <u>underscored</u> text
- `<code/>` - Marks text as `code`
- `<para/>` - Organises text into a paragraph, taking whitespaces and newlines into account
- `<list/>` and `<ordered-list/>` - Creates either an unordered or ordered-list of `<item/>` elements
- `<item/>` - Single list item
- `<link/>` - Contains 2 attributes: `type` and `href`. Type can be either `ext` (`href`=external URL link) or `slide` (`href`=position of a slide), which will link either to a website or another slide.

All of these elements can be also nested into each other to achieve the desired look.

### Parameters

The transformation `.xsl` file contains the following parameters for configuration:

- `text-font-size` - the font size of all `<content/>` elements of `type="text"` in pixels.
- `title-font-size`- the font size of the title in pixels.
- `subtitle-font-size`- the font size of the subtitle in pixels.
- `background-color` - the background color for all the slides, either as a word or a specific value.
